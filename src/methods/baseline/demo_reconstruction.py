"""A file containing a (pretty useless) reconstruction.
It serves as example of how the project works.
This file should NOT be modified.
"""


import numpy as np
from skimage.transform import rescale


def upscale_ms2(img: np.ndarray, shape: tuple) -> np.ndarray:
    """Upscales the ms2 acquisition to the shape of the desired reconstruction.

    Args:
        img (np.ndarray): ms2 image.
        shape (tuple): Shape of the output.

    Returns:
        np.ndarray: Upscaled image.
    """
    return rescale(img, 4, anti_aliasing=True, channel_axis=2)[:shape[0], :shape[1]]



####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
